import {
  NgModule,
  Optional,
  SkipSelf,
  ModuleWithProviders,
} from '@angular/core';
import { ApolloModule } from 'apollo-angular';
import { provideClient } from './graphql/client';
import { CommonModule } from '@angular/common';
import { PersonService } from './services/person.service';
import { RoomService } from './services/room.service';
import { ReservationService } from './services/reservation.service';
import { HttpClientModule } from '@angular/common/http';
import { EmailService } from './services/email.service';

/**
 * Checks if a module is imported more than once
 */
export function throwIfAlreadyLoaded(parentModule: any, moduleName: string) {
  if (parentModule) {
    throw new Error(
      `${moduleName} has already been loaded. Import Core modules in the AppModule only.`
    );
  }
}

@NgModule({
  imports: [CommonModule, [ApolloModule.forRoot(provideClient)]],
})
export class CoreModule {
  /**
   * Core Module can only being imported once
   * @param parentModule
   */
  constructor(
    @Optional()
    @SkipSelf()
    parentModule: CoreModule
  ) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }

  /**
   * All services imported in Code Module will be singleton
   * It means that the same instance will appear in the hole application
   * @returns {{ngModule: CoreModule, providers: Array}}
   */
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        ReservationService,
        RoomService,
        PersonService,
        EmailService
      ],
    };
  }
}
