import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

/**
 * Handles start and stop spinner methods
 */
@Injectable()
export class SpinnerService {
  private spinner = new BehaviorSubject<boolean>(false);
  spinner$ = this.spinner.asObservable();

  constructor() {}

  /**
   * Inits the spinner
   */
  start() {
    this.spinner.next(true);
  }

  /**
   * Stops the spinner
   */
  end() {
    this.spinner.next(false);
  }
}
