import { TestBed, inject } from '@angular/core/testing';

import { WizardService } from './wizard.service';

describe('WizardService', () => {
  let service: WizardService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WizardService],
    });
    service = TestBed.get(WizardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be 0', () => {
    expect(service.getCurrent()).toBe(0);
  });

  it('should be 1', () => {
    service.onNext();
    expect(service.getCurrent()).toBe(1);
  });

  it('should be 2', () => {
    service.onNext();
    service.onNext();
    expect(service.getCurrent()).toBe(2);
  });

  it('should be 1', () => {
    service.onNext();
    service.onNext();
    service.onPrevious();
    expect(service.getCurrent()).toBe(1);
  });

  it('should be 5', () => {
    service.setCurrent(5);
    expect(service.getCurrent()).toBe(5);
  });
});
