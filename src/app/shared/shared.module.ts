import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WizardComponent } from './components/statefull/wizard/wizard.component';
import { WizardService } from './services/wizard.service';
import { SpinnerService } from './services/spinner.service';
import { SpinnerComponent } from './components/statefull/spinner/spinner.component';
import { HttpModule } from '@angular/http';

@NgModule({
  imports: [CommonModule, HttpModule],
  declarations: [WizardComponent, SpinnerComponent],
  exports: [WizardComponent, SpinnerComponent],
  providers: [WizardService, SpinnerService],
})
export class SharedModule {}
