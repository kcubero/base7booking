import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WizardComponent } from './wizard.component';
import { WizardService } from '../../../services/wizard.service';
import { Step } from '../../../interfaces/step.interface';
import { By } from '@angular/platform-browser';
import { SharedModule } from '../../../shared.module';

describe('WizardComponent', () => {
  let component: WizardComponent;
  let fixture: ComponentFixture<WizardComponent>;
  let service: WizardService;
  const steps: Step[] = [
    {
      title: 'first',
    },
    {
      title: 'second',
    },
  ];

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [WizardComponent],
        providers: [WizardService],
      }).compileComponents();
      service = TestBed.get(WizardService);
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(WizardComponent);
    component = fixture.componentInstance;
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should be created with no steps', () => {
    component.ngOnInit();
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('ul > li'))).toBeNull();
  });

  it('should be created with no steps', () => {
    component.steps = steps;
    component.ngOnInit();
    fixture.detectChanges();
    const elements = fixture.debugElement.query(By.css('ul > li'));
    // console.log(elements);
    let counter = 0;

    // for (const element of elements) {
    //   counter++;
    // }
    // expect(fixture.debugElement.query(By.css('ul > li'))).toBe(2);
  });

  // TODO spec ngFor

  // TODO spec check current component printed

  // TODO spec ng-content
});
