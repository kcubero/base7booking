import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpinnerComponent } from './spinner.component';
import { SpinnerService } from '../../../services/spinner.service';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

describe('SpinnerComponent', () => {
  let component: SpinnerComponent;
  let fixture: ComponentFixture<SpinnerComponent>;
  let service: SpinnerService;
  let spinner: DebugElement;
  let overlay: DebugElement;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [SpinnerComponent],
        providers: [SpinnerService],
      }).compileComponents();
      service = TestBed.get(SpinnerService);
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(SpinnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    spinner = fixture.debugElement.query(By.css('#spinner'));
    overlay = fixture.debugElement.query(By.css('#overlay'));
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should not be loading', () => {
    expect(spinner.nativeElement.classList.contains('spinner')).toBeFalsy();
    expect(overlay.nativeElement.classList.contains('overlay-spinner')).toBeFalsy();
  });

  it('should be loading', () => {
    service.start();
    fixture.detectChanges();
    expect(spinner.nativeElement.classList.contains('spinner')).toBeTruthy();
    expect(overlay.nativeElement.classList.contains('overlay-spinner')).toBeTruthy();
  });

  it('should not be loading', () => {
    service.start();
    service.end();
    fixture.detectChanges();
    expect(spinner.nativeElement.classList.contains('spinner')).toBeFalsy();
    expect(overlay.nativeElement.classList.contains('overlay-spinner')).toBeFalsy();
  });
});
