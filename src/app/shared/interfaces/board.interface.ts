import { Room } from './room.interface';
export interface Board {
  id: string;
  type: string;
  price: number;
  rooms?: Room[];
  description: string;
}
