import { Component, OnInit, OnDestroy } from '@angular/core';
import { WizardService } from '../../../../shared/services/wizard.service';
import { PersonService } from '../../../../core/services/person.service';
import { Person } from '../../../interfaces/person.interface';
import { FormGroup } from '@angular/forms';
import { SelectedRoom } from '../../../interfaces/selected-room.interface';
import { RoomService } from '../../../../core/services/room.service';
import { ReservationService } from '../../../../core/services/reservation.service';
import { Subscription } from 'rxjs/Subscription';

/**
 * User fills a form with it's information to do a book
 */
@Component({
  selector: 'b7b-book-info',
  templateUrl: './book-info.component.html',
  styleUrls: ['./book-info.component.scss'],
})
export class BookInfoComponent implements OnInit, OnDestroy {
  supscriptions: Subscription[];
  selectedRooms: SelectedRoom[];
  reservation: FormGroup;

  constructor(
    private wizardService: WizardService,
    private personService: PersonService,
    private roomService: RoomService,
    private reservationService: ReservationService
  ) {}

  /**
   * Subscribe to room service and reservation service to get search object and selected rooms
   */
  ngOnInit() {
    this.supscriptions = [];

    this.supscriptions.push(
      this.roomService.room$.subscribe(
        res => (this.selectedRooms = res),
        // TODO track errors for stadistics
        err => console.log(err)
      )
    );

    this.supscriptions.push(
      this.reservationService.reservation$.subscribe(
        res => {
          this.reservation = this.reservationService.transformToForm(res);
        },
        // TODO track errors for stadistics
        err => console.log(err)
      )
    );
  }

  /**
   * Adds new value to person service and goes one step more in the wizard component
   * @param $event billing form
   */
  onFilledForm($event) {
    if ($event.action === 'BILLING') {
      this.personService.next(this.formToPerson($event.data.person));
      this.wizardService.onNext();
    }
  }

  /**
   * Transforms a FormGroup to an object with type Person
   * @param form billing form
   */
  private formToPerson(form: FormGroup): Person {
    return {
      firstName: form.get('firstName').value,
      lastName: form.get('lastName').value,
      address: {
        street: form.get('address').get('street').value,
        city: form.get('address').get('city').value,
        country: form.get('address').get('country').value,
      },
      email: form.get('email').value,
      phone: form.get('phone').value,
      notes: form.get('notes').value,
      payment: form.get('payment').value,
    };
  }

  /**
   * Unsubscribe from all Observables
   */
  ngOnDestroy() {
    this.supscriptions.map(subscription => subscription.unsubscribe());
  }
}
