import { Component, OnInit, OnDestroy } from '@angular/core';
import { PersonService } from '../../../../core/services/person.service';
import { Person } from '../../../interfaces/person.interface';
import { Subscription } from 'rxjs/Subscription';
import { EmailService } from '../../../../core/services/email.service';

/**
 * Confirmation component in booking process
 */
@Component({
  selector: 'b7b-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss'],
})
export class ConfirmationComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[];
  person: Person;
  loc: string;

  constructor(
    private personService: PersonService,
    private emailService: EmailService
  ) {}

  /**
   * Init variables
   */
  ngOnInit() {
    this.subscriptions = [];
    this.loc = this.generateRandomLoc();
    this.personService.person$.subscribe(
      res => {
        this.person = res;
        this.sendEmail();
      },
      // TODO track errors for stadistics
      err => console.log(err)
    );
  }

  /**
   * Generates a random loc
   */
  private generateRandomLoc() {
    return Math.random()
      .toString(36)
      .substring(2)
      .toUpperCase();
  }

  /**
   * Sends a confirmation email to the client
   */
  sendEmail() {
    const data = this.emailService.transformData(this.person, this.loc);
    this.emailService.sendEmail(data);
  }

  /**
   * Unsubscribe from all subscriptions
   */
  ngOnDestroy() {
    this.subscriptions.map(subscription => subscription.unsubscribe());
  }
}
