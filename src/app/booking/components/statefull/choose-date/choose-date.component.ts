import { Component, OnInit, OnDestroy } from '@angular/core';
import { WizardService } from '../../../../shared/services/wizard.service';
import { FormGroup } from '@angular/forms';
import { ReservationService } from '../../../../core/services/reservation.service';
import { Subscription } from 'rxjs/Subscription';

/**
 * Search form with rooms and dates
 */
@Component({
  selector: 'b7b-choose-date',
  templateUrl: './choose-date.component.html',
  styleUrls: ['./choose-date.component.scss'],
})
export class ChooseDateComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[];
  reservation: FormGroup;

  constructor(
    private wizardService: WizardService,
    private reservationService: ReservationService
  ) {}

  /**
   * Init reservation form from the service
   */
  ngOnInit() {
    this.subscriptions = [];

    this.subscriptions.push(
      this.reservationService.reservation$.subscribe(
        res =>
          (this.reservation = this.reservationService.transformToForm(res)),
        // TODO track errors for stadistics
        err => console.log(err)
      )
    );
  }

  /**
   * Adds new value to reservation ser
   * @param search form
   */
  onSearch($event: FormGroup) {
    if ($event) {
      this.reservationService.next(
        this.reservationService.transformToOject($event)
      );
      this.wizardService.onNext();
    }
  }

  /**
   * Unsubscribe from all subscriptions
   */
  ngOnDestroy() {
    this.subscriptions.map(subscription => subscription.unsubscribe());
  }
}
