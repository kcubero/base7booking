import { Component, OnInit, OnDestroy } from '@angular/core';
import { Step } from '../../../../shared/interfaces/step.interface';
import { WizardService } from '../../../../shared/services/wizard.service';
import { Subscription } from 'rxjs/Subscription';

/**
 * Parent component of booking process
 */
@Component({
  selector: 'b7b-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss'],
  providers: [WizardService],
})
export class BookingComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[];
  steps: Step[];
  current: number;

  constructor(private wizardService: WizardService) {}

  /**
   * Init wizard component
   */
  ngOnInit() {
    this.subscriptions = [];

    this.subscriptions.push(
      this.wizardService.current$.subscribe(
        res => {
          this.current = res;
        },
        // TODO track errors for stadistics
        err => console.log(err)
      )
    );

    this.steps = [
      { title: 'Availability' },
      { title: 'Choose Room' },
      { title: 'Booking Information' },
      { title: 'Booking Completed' },
    ];

    this.current = 0;
  }

  /**
   * Unsubscribe from all subscriptions
   */
  ngOnDestroy() {
    this.subscriptions.map(subscription => subscription.unsubscribe());
  }
}
