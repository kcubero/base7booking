import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomOptionComponent } from './room-option.component';

describe('RoomOptionComponent', () => {
  let component: RoomOptionComponent;
  let fixture: ComponentFixture<RoomOptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomOptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // TODO spec
  // it('should be created', () => {
  //   expect(component).toBeTruthy();
  // });
});
