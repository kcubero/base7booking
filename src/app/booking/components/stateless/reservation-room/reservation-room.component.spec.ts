import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationRoomComponent } from './reservation-room.component';

describe('ReservationRoomComponent', () => {
  let component: ReservationRoomComponent;
  let fixture: ComponentFixture<ReservationRoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservationRoomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationRoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
// TODO spec
  // it('should be created', () => {
  //   expect(component).toBeTruthy();
  // });
});
