import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

/**
 * Prints check-in and check-out dates and also total guests
 */
@Component({
  selector: 'b7b-reservation-selected-dates',
  templateUrl: './reservation-selected-dates.component.html',
  styleUrls: ['./reservation-selected-dates.component.scss'],
})
export class ReservationSelectedDatesComponent {
  @Input() reservation: FormGroup;

  constructor() {}
}
