import { Component, EventEmitter, Output, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import {
  NgbInputDatepicker,
  NgbDateStruct,
  NgbCalendar,
} from '@ng-bootstrap/ng-bootstrap';

/**
 * Search component where we can select the number of rooms, check-in and check-out dates
 */
@Component({
  selector: 'b7b-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.scss'],
})
export class ReservationComponent implements OnInit {
  @Output() search: EventEmitter<FormGroup> = new EventEmitter();
  @Input() searchForm: FormGroup;
  now: NgbDateStruct;
  endDate: NgbDateStruct;

  constructor(private fb: FormBuilder, private calendar: NgbCalendar) {}

  /**
   * Inits check-in and check-out dates
   * In case there is already a end date selected, we must specify min date
   */
  ngOnInit() {
    this.now = this.calendar.getToday();
    if (this.searchForm.get('dateFrom').value) {
      const end = this.searchForm.get('dateFrom').value;
      const date = new Date(`${end.year}/${end.month}/${end.day}`);
      date.setDate(date.getDate() + 1);
      this.endDate = {
        year: date.getFullYear(),
        month: date.getMonth() + 1,
        day: date.getDate(),
      };
    }
  }

  /**
   * Toggle dates to make a range input
   * Cannot do it in the view because it seems that min date does not update
   * @param start ngbDatepicker check-in date
   * @param end ngbDatepicker check-out date
   */
  onChange(
    $event: NgbDateStruct,
    start: NgbInputDatepicker,
    end: NgbInputDatepicker
  ) {
    start.close();
    const date = new Date(`${$event.year}/${$event.month}/${$event.day}`);
    date.setDate(date.getDate() + 1);
    end.minDate = {
      year: date.getFullYear(),
      month: date.getMonth() + 1,
      day: date.getDate(),
    };
    end.open();
  }

  /**
   * Emits to the parent component this search form
   */
  checkAvailability() {
    this.search.emit(this.searchForm);
  }

  /**
   * Every time the selected room dropdown changes,
   * we check if the current number of rooms is the same as the selected number of rooms
   */
  checkRooms() {
    const number = parseInt(this.searchForm.get('room').value);
    const rooms = this.searchForm.get('rooms').value.length;

    if (rooms < number) {
      this.addRooms(rooms, number);
    } else if (rooms > number) {
      this.removeRooms(rooms, number);
    }
  }

  /**
   * Adds a new FormGroup to rooms FormArray until the selected rooms number and the current rooms number are the same
   * @param rooms number of rooms
   * @param number number of rooms obtainet from searchForm.get('room')
   */
  private addRooms(rooms, number) {
    for (let i = rooms; i < number; i++) {
      (this.searchForm.get('rooms') as FormArray).push(this.addRoom());
    }
  }

  /**
   * Removes de last FormGroup of rooms until the selected rooms number and the current rooms number are the same
   * @param rooms number of rooms
   * @param number number of rooms obtainet from searchForm.get('room')
   */
  private removeRooms(rooms, number) {
    for (let i = rooms; i > number; i--) {
      const control = <FormArray>this.searchForm.controls['rooms'];
      control.removeAt(-1);
    }
  }

  /**
   * Returns a FormGroup of an object with adult and child properties
   * @returns {FormGroup}
   */
  private addRoom(): FormGroup {
    return this.fb.group({
      adult: 1,
      child: 0,
    });
  }
}
