import { Component } from '@angular/core';

/**
 * Shows static information to the user
 */
@Component({
  selector: 'b7b-extra-info',
  templateUrl: './extra-info.component.html',
  styleUrls: ['./extra-info.component.scss'],
})
export class ExtraInfoComponent {
  constructor() {}
}
