import { Component } from '@angular/core';

/**
 * 404 page
 */
@Component({
  selector: 'b7b-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss'],
})
export class NotFoundComponent {
  constructor() {}
}
