import gql from 'graphql-tag';
export const allRooms = gql`
  query {
    allRooms(filter: { hotel: { id: "cj7qdyzq87ibz012093akj9j8" } }) {
      id
      images
      description
      type
      services
      price
      board {
        id
        type
        description
        price
      }
    }
  }
`;
