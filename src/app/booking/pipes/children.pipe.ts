import { Pipe, PipeTransform } from '@angular/core';

/**
 * Children pipe to get total number of children
 */
@Pipe({
  name: 'children',
})
export class ChildrenPipe implements PipeTransform {
  /**
   * Returns children number
   * @param rooms FormArray
   */
  transform(rooms: any[]): number {
    if (rooms) {
      return rooms
        .map(x => parseInt(x.get('child').value))
        .reduce((prev, curr) => prev + curr);
    }

    return 0;
  }
}
