import { Pipe, PipeTransform } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';

/**
 * Transforms an NgbDateStruct to a Date Object
 */
@Pipe({
  name: 'ngbToDate',
})
export class NgbToDatePipe implements PipeTransform {
  /**
   * Returns a Date Object from a NgbDateStruct
   * @param value NgbDateStruct date
   * @returns {Date} JavaScript Date Object
   */
  transform(value: NgbDateStruct, format?: string): Date | string {
    if (value) {
      const date = new Date(`${value.year}/${value.month}/${value.day}`);
      if (format) {
        return DatePipe.prototype.transform(date, format);
      }
      return date;
    }
    return null;
  }
}
