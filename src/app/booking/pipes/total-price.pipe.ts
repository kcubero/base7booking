import { Pipe, PipeTransform } from '@angular/core';
import { SelectedRoom } from '../interfaces/selected-room.interface';

/**
 * Calculates total price
 */
@Pipe({
  name: 'totalPrice',
  pure: false,
})
export class TotalPricePipe implements PipeTransform {
  /**
   * Calculates the total price
   * @param selectedRoom array of selected rooms
   */
  transform(selectedRoom: SelectedRoom[]): number {
    let price = 0;
    if (selectedRoom && selectedRoom.length) {
      selectedRoom.forEach(x => {
        price = price + x.config.totalPrice;
      });
    }
    return price;
  }
}
