import { NgbToDatePipe } from './ngb-to-date.pipe';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

const date: NgbDateStruct = {
  year: 2018,
  month: 9,
  day: 21,
};
describe('NgbToDatePipe', () => {
  let pipe: NgbToDatePipe;

  beforeEach(() => {
    pipe = new NgbToDatePipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should return null', () => {
    expect(pipe.transform(null)).toBe(null);
  });

  it('should return same date', () => {
    const newDate = new Date(`${date.year}/${date.month}/${date.day}`);
    expect((<Date>pipe.transform(date)).getTime()).toBe(newDate.getTime());
  });

  it('should return short date', () => {
    expect(pipe.transform(date, 'short')).toBe('21/9/2018 0:00');
  });

  it('should return medium date', () => {
    expect(pipe.transform(date, 'medium')).toBe('21 sept. 2018 0:00:00');
  });
});
