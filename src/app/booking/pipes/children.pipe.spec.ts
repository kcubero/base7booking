import { ChildrenPipe } from './children.pipe';
import {
  FormGroup,
  FormControl,
  FormBuilder,
  ReactiveFormsModule,
  FormsModule,
} from '@angular/forms';
import { TestBed } from '@angular/core/testing';

describe('ChildrenPipe', () => {
  let pipe: ChildrenPipe;
  beforeEach(() => {
    pipe = new ChildrenPipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });
});
