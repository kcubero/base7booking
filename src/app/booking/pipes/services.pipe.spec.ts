import { ServicesPipe } from './services.pipe';

describe('ServicesPipe', () => {
  let pipe: ServicesPipe;

  beforeEach(() => {
    pipe = new ServicesPipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should return empty', () => {
    expect(pipe.transform(null)).toBe('');
  });

  it('should return empty', () => {
    expect(pipe.transform('')).toBe('');
  });

  it('should return wifi', () => {
    expect(pipe.transform('WIFI')).toBe('wifi');
  });

  it('should return glass', () => {
    expect(pipe.transform('BAR')).toBe('glass');
  });

});
