export interface SelectedRoom {
  adult: number;
  child: number;
  config: {
    type: string;
    mealplan: string;
    totalPrice: number;
  };
}
