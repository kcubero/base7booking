export interface Person {
  firstName: string;
  lastName: string;
  address: {
    street: string;
    city: string;
    country: string;
  };
  email: string;
  phone: string;
  notes?: string;
  payment: string;
}
