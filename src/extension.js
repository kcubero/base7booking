// Common class for book buttons
const buttonClass = 'fl-trailing';

/**
 * Waits until the window is loaded to listen click events
 * It is better to analyze each click than to add an event listener to each button since it is possible for the page to refresh or buttons are added or deleted
 * In this way we always make sure that the user is redirected to the popup
 */
window.onload = function() {
  document.addEventListener(
    'click',
    function(event) {
      if (event.target.classList.contains(buttonClass)) {
        // It's important to stop event propagation so the user is not able to go to the hotel page
        event.stopPropagation();
        event.preventDefault();
        openPopUp();
      }
    },
    true
  );
};

/**
 * Opens a full screen pop up with our app
 */
const openPopUp = () => {
  // It could be possible to pass parameters throught the URL:
  // const dates = getDates(); url + '?' + dates
  const url = chrome.extension.getURL('index.html');
  window.open(
    url,
    'base7booking',
    'width=' + screen.availWidth + ',height=' + screen.availHeight
  );
};

/**
 * We could send dates through query params to get hotel info, dates, etc.
 */
const getDates = () => {
  const checkIn = document
    .querySelector('.horus__col--checkin time')
    .getAttribute('datetime');
  const checkOut = document
    .querySelector('.horus__col--checkout time')
    .getAttribute('datetime');

  return `checkin=${checkIn}&checkout=${checkOut}`;
};
