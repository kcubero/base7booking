# Base7Booking

Chrome Extension for a booking process

## Getting Started

### Prerequisites

First install the LTS version of [Node.js](https://nodejs.org/es/).
Install the latest version of [Angular CLI](https://cli.angular.io/) via npm running `npm install -g @angular/cli`.

### Installing

Once downloaded the full repository, type `npm install`. It will start downloading all the necessary depencencies.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Deployment

### Generate build
To generate the build just type:
- `npm run ng -- build --prod`

### Build Chrome Extension
Once you've done the build, you'll need to add the extension to your browser. You've got an example below:
- Visit chrome://extensions in your browser
- Ensure that the Developer mode checkbox in the top right-hand corner is checked.
- Click Load unpacked extension
- Select dist folder from the project
- Enable Base7Booking extension

## Built With

* [Angular CLI](https://cli.angular.io/) - A command line interface for Angular
* [nb-bootstrap](https://ng-bootstrap.github.io/#/home) - Bootstrap 4 components, powered by Angular
* [Font Awesome](http://fontawesome.io/) - The ionic font and css toolkit
* [SASS](http://sass-lang.com/) - CSS extension language
* [Graphcool](https://www.graph.cool/) - Serverless GraphQL Backend 

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the tags on this repository.

## Authors

* **Karen Cubero González**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
